package net.chenlin.dp.ids.admin.core.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 基础dao
 * @author zcl<yczclcn@163.com>
 */
public interface BaseMapper<T> {
	
	/**
	 * 新增
	 * @param t
	 * @return
	 */
	int insert(T t);
	
	/**
	 * 新增
	 * @param map
	 * @return
	 */
	int insertByMap(Map map);
	
	/**
	 * 批量新增
	 * @param items
	 * @return
	 */
	int batchInsert(List<T> items);
	
	/**
	 * 查询详情
	 * @param map
	 * @return
	 */
	T getByMap(Map map);
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	T getById(Serializable id);
	
	/**
	 * 更新
	 * @param t
	 * @return
	 */
	int update(T t);
	
	/**
	 * 更新
	 * @param map
	 * @return
	 */
	int updateByMap(Map map);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	int delete(Serializable id);
	
	/**
	 * 逻辑删除
	 * @param id
	 * @return
	 */
	int deleteLogic(Serializable id);
	
	/**
	 * 批量删除
	 * @param id
	 * @return
	 */
	int deleteBatchIds(Serializable[] id);
	
	/**
	 * 批量逻辑删除
	 * @param id
	 * @return
	 */
	int deleteLogicBatchIds(Serializable[] id);
	
	/**
	 * 分页查询列表
	 * @param page
	 * @param map
	 * @return
	 */
	Page<T> listForPage(Page<T> page, Map map);
	
	/**
	 * 查询列表
	 * @param map
	 * @return
	 */
	List<T> listByMap(Map map);
	
	/**
	 * 查询列表
	 * @return
	 */
	List<T> list();
	
	/**
	 * 统计
	 * @return
	 */
	int count();
	
	/**
	 * 统计
	 * @param map
	 * @return
	 */
	int countByMap(Map map);

}
