package net.chenlin.dp.ids.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * json工具类
 * @author zcl<yczclcn@163.com>
 */
public class JsonUtil {

    /**
     * json转字符串
     * @param obj
     * @return
     */
    public static String toStr(Object obj) {
        if (obj != null) {
            return JSONObject.toJSONString(obj, SerializerFeature.WRITE_MAP_NULL_FEATURES);
        }
        return null;
    }

    /**
     * json转字符串，带日期转换格式
     * @param obj
     * @param dateFormat
     * @return
     */
    public static String toStr(Object obj, String dateFormat) {
        if (obj != null) {
            if (CommonUtil.strIsEmpty(dateFormat)) {
                return JSONObject.toJSONString(obj);
            }
            return JSON.toJSONStringWithDateFormat(obj, dateFormat);
        }
        return null;
    }

    /**
     * json转对象
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObj(String json, Class<T> clazz) {
        if (CommonUtil.strIsEmpty(json) || clazz == null) {
            return null;
        }
        return JSONObject.parseObject(json, clazz);
    }

}
